FROM golang:1.21-alpine as builder
WORKDIR /go/src/echoserver-go

COPY go.mod go.sum ./
COPY main.go .

RUN CGO_ENABLED=0 go build -ldflags="-w -s" -o /go/bin/echoserver

FROM gcr.io/distroless/static
COPY --from=builder /go/bin/echoserver /opt/echoserver

EXPOSE 3000

ENTRYPOINT ["/opt/echoserver"]
