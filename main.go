package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/gorilla/handlers"
)

func echo(w http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()
	if err != nil {
		log.Printf("error parsing form: %s", err.Error())
	}

	headerNames := make([]string, 0, len(req.Header))
	for name, _ := range req.Header {
		headerNames = append(headerNames, name)
	}
	sort.Strings(headerNames)
	for _, name := range headerNames {
		fmt.Fprintf(w, "%v: %v\n", name, strings.Join(req.Header[name], ","))
	}

	fmt.Fprint(w, "\n")
	fmt.Fprintf(w, "%v\n", req.Method)
	fmt.Fprintf(w, "%v\n", req.Proto)
	fmt.Fprintf(w, "%v\n", req.RequestURI)
	fmt.Fprintf(w, "%v\n", req.RemoteAddr)
	fmt.Fprintf(w, "%v\n", req.Host)

	for name, values := range req.Form {
		fmt.Fprint(w, "\n")
		for index := range values {
			fmt.Fprintf(w, "%v = %v", name, values[index])
		}
	}

	rawBody, err := io.ReadAll(req.Body)
	if err != nil {
		log.Printf("error reading body: %v", err)
		http.Error(w, "error reading body", http.StatusBadRequest)
		return
	}

	var jsonBody map[string]interface{}
	if err := json.Unmarshal(rawBody, &jsonBody); err != nil {
		fmt.Fprint(w, "\n")
		fmt.Fprintf(w, "%v\n", string(rawBody))
		return
	}

	body, err := json.MarshalIndent(jsonBody, "", "  ")
	if err != nil {
		log.Printf("error formatting body %v", err)
		http.Error(w, "error formatting body", http.StatusBadRequest)
		return
	}
	if string(body) != "" {
		fmt.Fprint(w, "\n")
		fmt.Fprintf(w, "%v\n", string(body))
	}
}

func main() {
	http.HandleFunc("/", echo)

	server := &http.Server{
		Addr:        ":3000",
		ReadTimeout: 3 * time.Second,
		Handler:     handlers.LoggingHandler(os.Stdout, http.DefaultServeMux),
	}
	fmt.Println("Server started on port 3000...")
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
