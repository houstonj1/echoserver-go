module gitlab.com/houstonj1/echoserver-go

go 1.21

require github.com/gorilla/handlers v1.5.2

require github.com/felixge/httpsnoop v1.0.3 // indirect
